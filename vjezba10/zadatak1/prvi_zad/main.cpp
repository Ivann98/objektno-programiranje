#include "stack.h"

int main()
{
	Stack<int> s(5);

	s.push(111);
	s.push(3);
	s.push(77);

	std::cout << s.pop();
	std::cout << s.pop();
	std::cout << s.pop();

}