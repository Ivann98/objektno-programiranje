#pragma once
#include <iostream>

template <class T>

class Stack
{
private:
	T* p;
	int sp, kapacitet;
public:
	Stack(int size);
	bool is_stack_empty();
	bool is_stack_full();
	void push(T elem);
	T pop();
};


template<class T>
inline Stack<T>::Stack(int size)
{
	p = new T[size];
	this->kapacitet = size;
	sp = -1;
}

template<class T>
inline void Stack<T>::push(T elem)
{
	if (is_stack_full())
	{
		std::cout << "Stack je prepunjen!";
		exit(EXIT_FAILURE);
	}
	else
	{
		p[++sp] = elem;
	}
}


template<class T>
inline bool Stack<T>::is_stack_empty()
{
	if (sp == -1)
	{
		return true;
	}
	return false;
}

template<class T>
inline bool Stack<T>::is_stack_full()
{
	if (sp == kapacitet - 1)
	{
		return true;
	}
	return false;
}