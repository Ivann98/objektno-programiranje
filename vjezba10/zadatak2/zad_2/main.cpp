#include <iostream>

template<typename T>
void sort(T arr[], int size)
{
	int i, j;
	for (i = 0; i < size - 1; i++)
	{
		for (j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				auto temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

template<>
void sort<char>(char arr[], int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (std::tolower(arr[j]) > std::tolower(arr[j + 1]))
			{
				char temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

int main()
{
	float niz[5] = { 4.6, 2.1,1.2,4.5,5 };
	sort(niz, 5);

	/*for (int i = 0; i < 5; i++)
	{
		std::cout << niz[i] << " ";
	}*/

	char arr[5] = { 'A','c','a','b','B' };
	sort<char>(arr, 5);
	for (int i = 0; i < 5; i++)
	{
		std::cout << arr[i] << " ";
	}

}