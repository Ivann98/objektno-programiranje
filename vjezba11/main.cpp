#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>

std::vector<int> fill_vector()
{
    std::vector<int> vect;
    std::ifstream file("numbers.txt");
    if (!file.is_open())
    {
        throw - 1;
    }
    std::istream_iterator<int> is(file), eos;
    std::vector<int>::iterator end;
    copy(is, eos, back_inserter(vect));

    return vect;
}


int main()
{
    std::vector<int> vect;
    try
    {
        vect = fill_vector();
        int brojevi = std::count_if(begin(vect), end(vect), [](int number) {return number > 500; });

        std::cout << brojevi;

        auto min = std::min_element(begin(vect), end(vect));
        auto max = std::max_element(begin(vect), end(vect));

        std::cout << "min: " << *min << " max: " << *max << std::endl;
        vect.erase(std::remove_if(begin(vect), end(vect), [](int number) {return number < 300; }), vect.end());

        std::sort(begin(vect), end(vect), [](int number1, int number2) {return number2 < number1; });

        std::ostream_iterator<int> os(std::cout, ",");

        copy(begin(vect), end(vect), os);
    }
    catch (int)
    {
        std::cout << "Error pri otvaranju datoeke. File ne postoji!";
    }
}