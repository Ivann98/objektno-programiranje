#include "formula.h"
#include <algorithm>

float Formula::getAverageSpeed()
{

	Timer average = { 0,0,0 };

	for (auto const& it : this->times)
	{
		average += it;
	}
	float temp = average.getHour() * 60 * 60 + average.getMinute() * 60 + average.getSeconds() * 60;
	temp /= times.size();

	return temp;
}

Timer Formula::getFastestDriver()
{
	auto it = std::min_element(begin(times), end(times));

	Timer fastest = *it;

	return fastest;
}

std::vector<float> Formula::getDifference()
{
	Timer fastest = getFastestDriver();
	std::vector<float> difference;

	float max = fastest.getHour() * 3600 + fastest.getMinute() * 60 + fastest.getSeconds();
	for (auto it : times)
	{
		float time = it.getHour() * 3600 + it.getMinute() * 60 + it.getSeconds();
		if (fastest - time != 0)
			difference.push_back(fastest - time);
	}
	return difference;
}

void Formula::printTimes()
{
	for (auto const& it : times)
	{
		std::cout << it << std::endl;
	}
}

void Formula::punish(Timer m)
{
	for (auto& it : times)
	{
		it += m;
	}
}

void Formula::punishSeconds(int m)
{
	for (auto& it : times)
	{
		it.addSeconds(m);
	}
}
