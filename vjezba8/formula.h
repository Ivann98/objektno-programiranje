#pragma once
#include "timer.h"
#include <vector>

class Formula
{
private:
	std::vector<Timer> times;
public:

	Formula(std::vector<Timer> times) : times(times) {}
	float getAverageSpeed();
	Timer getFastestDriver();
	std::vector<float> getDifference();
	void printTimes();
	void punish(Timer m);
	void punishSeconds(int m);
};
