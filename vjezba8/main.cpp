#include "formula.h"


int main()
{
	std::vector<Timer> times;

	Timer t1(1, 5, 2);
	Timer t2(2, 6, 3);
	Timer t3(3, 7, 4);

	times.push_back(t1);
	times.push_back(t2);
	times.push_back(t3);

	Formula f(times);

	f.printTimes();

	f.punishSeconds(10);
	std::cout << "\n";
	f.printTimes();


	std::vector<float> fastest = f.getDifference();

	for (auto it : fastest)
	{
		std::cout << it << "\n";
	}

}