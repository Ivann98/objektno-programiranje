#include "timer.h"


Timer operator+(Timer const& t1, Timer const& t2)
{
	Timer t(t1.h + t2.h, t1.m + t2.m, t1.s + t2.s);

	return t;
}

Timer operator-(Timer const& t1, Timer const& t2)
{
	Timer t(t1.h - t2.h, t1.m - t2.m, t1.s - t2.s);

	return t;

}

Timer operator/(Timer const& t1, Timer const& t2)
{
	Timer t(t1.h / t2.h, t1.m / t2.m, t1.s / t2.s);
	return t;
}

std::ostream& operator<<(std::ostream& os, const Timer& t)
{
	os << "H: " << t.h << " M: " << t.m << " S: " << t.s;

	return os;
}

bool operator<(const Timer& t1, const Timer& t2)
{

	return std::tie(t1.h, t1.m, t1.s) < std::tie(t2.h, t2.m, t2.s);
}

Timer& Timer::operator+=(Timer const& t)
{
	this->h += t.h;
	this->m += t.m;
	this->s += t.s;

	return *this;
}

Timer& Timer::operator-=(Timer const& t)
{
	this->h -= t.h;
	this->m -= t.m;
	this->s -= t.s;

	return *this;
}

Timer Timer::operator/=(Timer const& t)
{
	this->h /= t.h;
	this->m /= t.m;
	this->s /= t.s;

	return *this;
}

Timer::operator double()
{

	double sec = h * 3600 + m * 60 + s;
	sec = sec + ((0.0) / 100);
	return sec;
}
