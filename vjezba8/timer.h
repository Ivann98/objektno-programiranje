#pragma once
#include <iostream>
#include <tuple>

class Timer
{
private:
	int h, m;
	double s;
public:

	Timer() {}
	Timer(int h, int m, double s) : h(h), m(m), s(s) {}

	/*Timer(int h, int m, double s)
	{

		this->m = m;
		this->h = h;
		this->s = s;

	}*/

	friend Timer operator+(Timer const&, Timer const&);
	friend Timer operator-(Timer const&, Timer const&);

	Timer& operator +=(Timer const&);
	Timer& operator -=(Timer const&);

	friend Timer operator/(Timer const&, Timer const&);
	Timer operator/=(Timer const&);

	friend std::ostream& operator<<(std::ostream& os, const Timer&);

	friend bool operator<(const Timer&, const Timer&);

	operator double();

	int getHour() { return this->h; }
	int getMinute() { return this->m; }
	double getSeconds() { return this->s; }

	//void setHour(int h) { this->h = h; }
	//void setMinutes(int m) { this->m = m; }
	//void setSeconds(double s) { this->s = s; }


	void addSeconds(double s) { this->s = this->s + s; }
};

