#pragma once
#include "Player.h"
#include <iostream>

using namespace std;

class HumanPlayer : public Player {
protected:
	string igrac = "covjek";
public:
	HumanPlayer() : Player() {}
	string player_type() { return this->igrac; }
	~HumanPlayer() {}
};
