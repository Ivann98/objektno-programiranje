#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ComputerPlayer.h"
#include "HumanPlayer.h"
#include "Game.h"
#include "Player.h"

using namespace std;

int Game::add_picked_numbers(int ary[], int &add) {
	int i=0, n, number;
	cout << "\nUnesite broj 1, 2 ili 3:";
	cin >> n;
	while (i < n) {
		number = pick_random(ary);
		add += number;
		i++;
	}
	return add;
}

int Player::guess_number() {
	int number;
	cout << "\nUnesite broj za provjeru:" << endl;
	cin >> number;
	//cout << number<<endl;
	return number;
}

void Player::pick_player() {
	int protivnik;
	cout << "\nUnesite protiv koga zelite igrat, 1=covjek ili 2=racunalo:";
	cin >> protivnik;
	if (protivnik == 1) {
		cout << "Odabran protivnik je covjek";
	}
	else {	
		cout << "Odabran protivnik je racunalo";
		
	}
}

int Game::pick_random(int ary[]) {
	//Player b;
	int random = ary[rand() % 2];
	//b.Victory(b.wins);
	cout << "\nIzabrani: " << random;
	return random;
}

void Player::Victory(int &wins) {
	Game n;
	int entry=guess_number();
	int num = n.add_picked_numbers(n.ary, n.add);
	cout << "\nzbroj je:" << num;
	if (num == entry)
	{
		wins++;
		cout << "\nPogodeni do sad: "<<wins;
	}
	if (wins == n.victory)
		cout << "\nPobjeda!"<<endl;
}

int main()
{
	srand((int)time(NULL));
	Game a;
	Player b;
	b.pick_player();
	while (b.wins!=a.victory) {
		b.Victory(b.wins);
	}
	cout << "\npogodeni do sad: " << b.wins;
	return 0;
}