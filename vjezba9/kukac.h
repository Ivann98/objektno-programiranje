#pragma once
#include "zivotinja.h"

class Kukac : public Zivotinja
{
protected:
	int legs;
public:

	Kukac() : Zivotinja()
	{
		this->legs = 4;
	}
	Kukac(int legs) : Zivotinja()
	{
		this->legs = legs;

	}
	int getNumberOfLegs() { return this->legs; }

	~Kukac() {}

};
