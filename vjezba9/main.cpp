#include "zohar.h"
#include "tarantula.h"
#include "vrabac.h"
#include<vector>
#include<iostream>


int getNumberOfAllLegs(std::vector<Zivotinja*> animals)
{
	int counter = 0;

	for (auto it : animals)
	{
		counter += (*it).getNumberOfLegs();
	}
	return counter;
}

int main()
{
	std::vector<Zivotinja*> animals;

	Zohar p(6);
	Zivotinja* ziv = (&p);
	animals.push_back(ziv);

	Tarantula t(8);
	Zivotinja* ziv2 = (&t);
	animals.push_back(ziv2);

	Vrabac v(2);
	Zivotinja* ziv3 = (&v);
	animals.push_back(ziv3);


	for (auto it : animals)
	{
		std::cout << (*it).getSpecies() << std::endl;
	}

	std::cout << getNumberOfAllLegs(animals);
}