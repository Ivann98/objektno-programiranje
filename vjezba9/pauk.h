#pragma once
#include "zivotinja.h"

class Pauk : public Zivotinja
{
protected:
	int legs;
public:

	Pauk() : Zivotinja()
	{
		this->legs = 8;
	}
	Pauk(int legs) : Zivotinja()
	{
		this->legs = legs;
	}

	int getNumberOfLegs() { return this->legs; }

	~Pauk() {}
}; 
