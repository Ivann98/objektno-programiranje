#pragma once
#include "zivotinja.h"

class Ptica : public Zivotinja
{
protected:
	int legs;
public:

	Ptica() : Zivotinja()
	{
		this->legs = 2;
	}
	Ptica(int legs) : Zivotinja()
	{
		this->legs = legs;

	}
	int getNumberOfLegs() { return this->legs; }

	~Ptica() {}
}; 
