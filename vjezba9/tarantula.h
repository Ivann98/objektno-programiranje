#pragma once
#include "pauk.h"

class Tarantula : public Pauk
{
protected:
	std::string type = "Tarantula";
public:

	Tarantula() : Pauk() {}
	Tarantula(int broj) : Pauk(broj) {}
	std::string getSpecies() { return this->type; }
	~Tarantula() {}
}; 
