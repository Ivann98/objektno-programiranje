#pragma once
#include "ptica.h"

class Vrabac : public Ptica
{
protected:
	std::string type = "Vrabac";
public:

	Vrabac() : Ptica() {}
	Vrabac(int broj) : Ptica(broj) {}
	std::string getSpecies() { return this->type; }
	~Vrabac() {}
};