#pragma once
#include <string>

class Zivotinja
{
private:
public:
	Zivotinja() {}
	virtual ~Zivotinja() {}
	virtual int getNumberOfLegs() = 0;
	virtual std::string getSpecies() = 0;
};
