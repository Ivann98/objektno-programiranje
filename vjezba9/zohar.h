#pragma once
#include "kukac.h"


class Zohar : public Kukac
{

protected:
	std::string type = "zohar";
public:

	Zohar() : Kukac() {}
	Zohar(int broj) : Kukac(broj) {}
	std::string getSpecies() { return this->type; }
	~Zohar() {}

};
