#include "header.h"


using namespace std;

void unos (float **matrix, struct matrica x){
	cout<<"unesite clanove matrice"; 
    for (int i=0; i<x.r; i++) {
        for (int j=0; j<x.s; j++)
            cin>>matrix[i][j];
    }
}

void ispis_odu_zbr (float **matrix_zbr, float **matrix_odu, struct matrica x){
	cout<<"\nRezultat zbrajanja matrica";
	for (int i=0; i<x.r; i++){
        cout<<endl;
		for (int j=0; j<x.s; j++){
	        cout<<matrix_zbr[i][j];
			cout<<" ";
		}
	}
	cout<<"\nRezultat oduzimanja matrica";
	for (int i=0; i<x.r; i++){
        cout<<endl;
		for (int j=0; j<x.s; j++){
			cout<<matrix_odu[i][j];		
			cout<<" ";
		}
	}
}

int mno (float **matrix, float **matrix2, float **matrix_mno, struct matrica x){
    int sum=0;
	if (x.s==x.r2){
    	for (int i=0; i<x.r; i++){
    		for (int j=0; j<x.s2; j++){
    			for (int k=0; k<x.r2; k++)
    				sum+=matrix[i][k]*matrix2[k][j];
			matrix_mno[i][j]=sum;
			sum=0;
			}
		}
	}
}
	
int zbr_odu (float **matrix, float **matrix2, float **matrix_zbr, float **matrix_odu, struct matrica x){
	if ((x.r==x.r2) && (x.s==x.s2)){
        for (int i=0; i<x.r; i++) {
            for (int j=0; j<x.s; j++){
                matrix_zbr[i][j]=matrix[i][j]+matrix2[i][j];
                matrix_odu[i][j]=matrix[i][j]-matrix2[i][j];
            }
        }
    	ispis_odu_zbr (matrix_zbr, matrix_odu, x);
	}
    else{
        cout<<"Matrice se ne mogu zbrajat niti oduzimati"<<endl;
		return 0;
	}
}

void mnozenje (float **matrix_mno, struct matrica x) {
	if (x.s==x.r2){
		cout<<"\nRezultat mnozenja matrica";
		for (int i=0; i<x.r; i++){
        	cout<<endl;
			for (int j=0; j<x.s2; j++){
				cout<<matrix_mno[i][j];		
				cout<<" ";
			}
		}
	}
}

void upis_a_b (float **matrix, int a, int b, struct matrica x){
    cout<<"unesite a i b"<<endl;
    cin>>a>>b;
        while (1)
        {
            if (abs((b-a))+1>=(x.r*x.s))
                break;
            else   
                {
                cout<<"ne valjan unos, ponoviti"<<endl;
                cin>>a>>b;
                }
        }
    for (int i=0; i<x.r; i++){
        for (int j=0; j<x.s; j++){
            if (a<=b){
                matrix[i][j]=a;
                a++;
            }
        }         
    }
}

void transponirana (float **matrix, float **matrix_tran, struct matrica x){
	for (int i=0; i<x.r; i++){
        for (int j=0; j<x.s; j++)
			matrix_tran[j][i]=matrix[i][j];
	}
	cout<<"\nTransponirana matrica je: ";
	for (int i=0; i<x.s; i++){
        cout<<endl;
		for (int j=0; j<x.r; j++){
			cout<<matrix_tran[i][j];
			cout<<" ";
		}
	}
}

void ispis (float **matrix, float **matrix_tran, struct matrica x){
    cout<<"\nMatrica je: ";
	for (int i=0; i<x.r; i++){
        cout<<endl;
		for (int j=0; j<x.s; j++){
			cout<<matrix[i][j];
    		cout<<" ";
		}
	}
}


