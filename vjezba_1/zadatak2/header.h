#ifndef HEADER_H
#define HEADER_H
#include <iostream>
#include <cstdlib>

using namespace std;

struct matrica {
	int r, s, r2, s2;
};

void unos (float **matrix, struct matrica x);

void ispis_odu_zbr (float **matrix_zbr, float **matrix_odu, struct matrica x);

int mno (float **matrix, float **matrix2, float **matrix_mno, struct matrica x);

int zbr_odu (float **matrix, float **matrix2, float **matrix_zbr, float **matrix_odu, struct matrica x);

void mnozenje (float **matrix_mno, struct matrica x);

void upis_a_b (float **matrix, int a, int b, struct matrica x);

void transponirana (float **matrix, float **matrix_tran, struct matrica x);

void ispis (float **matrix, float **matrix_tran, struct matrica x);

#endif
