#include <iostream>
#include "header.h"


using namespace std;

int main()
{
    struct matrica x;
	int a, b;
    float **matrix, **matrix2, **matrix_zbr, **matrix_odu, **matrix_mno, **matrix_tran;
    cout<<"unesite broj redaka i stupaca";
    cin>>x.r>>x.s;
    cout<<"unesite broj redaka i stupaca za drugu matricu";
    cin>>x.r2>>x.s2;

    matrix=new float*[x.r];
    for (int i=0; i<x.r; i++)
        matrix[i]=new float[x.s];
    
    matrix2=new float*[x.r2];
    for (int i=0; i<x.r2; i++)
        matrix2[i]=new float[x.s2];
    
    matrix_zbr=new float*[x.r];
    for (int i=0; i<x.r; i++)
        matrix_zbr[i]=new float[x.s2];
    
    matrix_odu=new float*[x.r];
    for (int i=0; i<x.r; i++)
        matrix_odu[i]=new float[x.s2];

	matrix_mno=new float*[x.r];
	for (int i=0; i<x.r; i++)
		matrix_mno[i]=new float[x.s2];
		
	matrix_tran=new float*[x.s];
	for (int i=0; i<x.s; i++)
		matrix_tran[i]=new float[x.r];

/*    unos (matrix, x);
    unos (matrix2, x);
	zbr_odu (matrix, matrix2, matrix_zbr, matrix_odu, x);
    upis_a_b (matrix, a, b, x);
    ispis (matrix, matrix_tran, x);
    //ispis (matrix2, matrix_tran, x);
    mnozenje (matrix_mno, x);
	mno (matrix, matrix2, matrix_mno, x);
	transponirana (matrix, matrix_tran, x);*/

	for (int i=0; i<x.r; i++)
        delete [] matrix[i];
	  delete [] matrix;
	
	for (int i=0; i<x.r2; i++)
        delete [] matrix2[i];
	  delete [] matrix2;
	
	for (int i=0; i<x.r; i++)
        delete [] matrix_zbr[i];
	  delete [] matrix_zbr;
        
    for (int i=0; i<x.r; i++)
        delete [] matrix_odu[i];
	  delete [] matrix_odu;
    
	for (int i=0; i<x.r; i++)
       delete [] matrix_mno[i];
	  delete [] matrix_mno;
    
	for (int i=0; i<x.s; i++)
        delete [] matrix_tran[i];
	  delete [] matrix_tran;

}

