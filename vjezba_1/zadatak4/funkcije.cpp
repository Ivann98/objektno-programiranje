#include "header.h"

using namespace std;


int *unos (int *niz, int n)
{
    for (int i=0; i<n; i++)
    {
        cout<<"unesite "<<i+1<<" clan niza ";
        cin>>niz[i];
    }
    return niz;
}

int *rekurzija (int *niz, int n, int *min, int *max)
{
    int i=n-1;
    if (i<0)
        return 0;
    if (niz[i]<*min)
        *min=niz[i];
    if (niz[i]>*max)
        *max=niz[i];
    return rekurzija (niz, n-1, min, max);
}
