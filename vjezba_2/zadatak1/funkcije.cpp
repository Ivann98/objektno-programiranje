#include <iostream>
#include <cstdlib>
#include "header.h"

using namespace std;

void unos_niza(int* niz, int n) 
{
	cout << "Unesite clanove niza:";
	for (int i = 0; i < n; i++)
		cin >> niz[i];
}

void min_max(int *niz, int& min, int& max, int n)
{
	for (int i = 0; i < n; i++)
	{
		if (niz[i] < min)
			min = niz[i];
		if (niz[i] > max)
			max = niz[i];
	}
}
