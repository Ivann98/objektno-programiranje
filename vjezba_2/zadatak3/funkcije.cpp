#include <iostream>
#include <cstdlib>
#include "header.h"

using namespace std;


struct pravokutnik {
	int x1, y1, x2, y2;
};

struct kruznica {
	int polumjer, x, y;
};

int presjecanje(pravokutnik p, kruznica k)
{
	int v, s, ux, uy, brojac=0;
	v=abs(p.x2-p.x1)/2;
	s=abs(p.y1-p.y2)/2;
	ux=abs(k.x-p.x1);
	uy=abs(k.y-p.y1);
	if (ux<=v || uy<=s)
		brojac++;
	return brojac;
}
