#ifndef HEADER.H
#define HEADER.H
#include <cstdlib>
#include <iostream>

using namespace std;

struct pravokutnik {
	int x1, y1, x2, y2;
};

struct kruznica {
	int polumjer, x, y;
};

int presjecanje(pravokutnik p, kruznica k);

#endif
