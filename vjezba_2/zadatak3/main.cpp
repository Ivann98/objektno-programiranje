#include <iostream>
#include <cstdlib>
#include "header.h"

using namespace std;

int main() 
{
	struct pravokutnik *p;
	struct kruznica k;
	int n, b=0, a;
	cout << "\nUnesite broj pravokutnika";
	cin >> n;
	p = new pravokutnik[n];
	cout << "unesite polumjer kruznice i srediste, x i y";
	cin >> k.polumjer >> k.x >> k.y;
	cout << "unesite koordinate pravokutnika, za x1, y1 i x2, y2";
	for (int i = 0; i < n; i++)
	{
		cin >> p[i].x1 >> p[i].y1;
		cin >> p[i].x2 >> p[i].y2;
		b=presjecanje (p[i], k);
		a+=b;
	}
	cout <<"Broj pravokutnika koji se presjecaju s kruznincom: "<<a;
	delete[] p;
	return 0;
}

