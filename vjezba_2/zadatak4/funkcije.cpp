#include <iostream>
#include <cstdlib>
#include "header.h"

using namespace std;


struct Vektor{
        int *niz;
        size_t log_vel, fiz_vel;
        
        void vector_new (int init);
        void vector_del (Vektor &novi);
        void push_back (Vektor novi, double &broj, int init);
        void pop_back (Vektor &novi);
        int vector_front (Vektor novi); 
        int vector_back (Vektor novi);
        size_t vector_size (Vektor novi);

		void print_vector();


void Vektor::vector_new (int init)
{
    Vektor nova;
    nova.niz=new int[init];
    nova.fiz_vel=init;
    nova.log_vel=0;
}

void Vektor::vector_del (Vektor &novi)
{
    delete [] novi.niz;
    novi.fiz_vel=0;
    novi.log_vel=0;
    
}

//provjeriti je li niz ispunjen i uve�at za duplo ako je, svaki put 

void Vektor::push_back (Vektor novi, int &broj, int init)
{
    int i;
	if (novi.log_vel==novi.fiz_vel){
		int *temp=novi.niz;
		novi.niz=new int[2*novi.fiz_vel];
		for (i=0; i<novi.fiz_vel; i++)
        	novi.niz[i]=temp[i];
    }
    delete [] temp;
    novi.fiz_vel++;
}

void Vektor::pop_back (Vektor &novi)
{
    novi.fiz_vel=novi.fiz_vel-1;
}

int Vektor::vector_front (Vektor novi)
{
    return novi.niz[0];
}

int Vektor::vector_back (Vektor novi)
{
    return novi.niz[novi.fiz_vel-1];
}

int Vektor::vector_size (Vektor novi)
{
    if (novi.fiz_vel==0)
        return -1;
    return novi.fiz_vel;
}





