#ifndef HEADER_H
#define HEADER_H
#include <cstdlib>
#include <iostream>

using namespace std;

struct Vektor{
        int *niz;
        size_t log_vel, fiz_vel;
        
        void vector_new (int init);
        void vector_del (Vektor &novi);
        void push_back (Vektor novi, double &broj, int init);
        void pop_back (Vektor &novi);
        int vector_front (Vektor novi); 
        int vector_back (Vektor novi);
        size_t vector_size (Vektor novi);

		void print_vector();
};

void Vektor::vector_new (int init);

void Vektor::vector_del (Vektor &novi);

void Vektor::push_back (Vektor novi, int &broj, int init);

void Vektor::pop_back (Vektor &novi);

int Vektor::vector_front (Vektor novi);

int Vektor::vector_back (Vektor novi);

int Vektor::vector_size (Vektor novi);

#endif
