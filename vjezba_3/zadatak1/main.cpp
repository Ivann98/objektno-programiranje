#include <iostream>
#include <vector>
#include <cstdlib>
#include "header.h"

using namespace std;

int main()
{
    int ele_number, a, b;
    vector<int> myvector;

    cout<<"unesite broj elemenata vektora";
    cin>>ele_number;
    
    num_of_elements(myvector, ele_number);
    for (unsigned int i=0; i<myvector.size(); i++)
        cout<<myvector[i]<<" ";
    cout<<"\n"<<myvector.capacity();
    
    cout<<"unesite donju i gornju granicu";
    cin>>a>>b;
    interval(myvector, a, b);
    for (unsigned int i=0; i<myvector.size(); i++)
        cout<<myvector[i]<<" ";
    cout<<"\n"<<myvector.capacity();
    return 0;
}

