#ifndef HEADER_H
#define HEADER_H
#include <cstdlib> 
#include <iostream>
#include <vector>


void num_of_elements (vector<int> &myvector, int ele_number);

void interval (vector<int> &myvector, int a, int b);

void third_vector (vector<int> &v1, vector<int> &v2, vector<int> &v3, int ele_number);

#endif
