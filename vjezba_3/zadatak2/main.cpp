#include <iostream>
#include <vector>
#include <cstdlib>
#include "header.h"


int main()
{
    int ele_number, a, b;
    vector<int> v1, v2, v3;

    cout<<"unesite broj elemenata vektora";
    cin>>ele_number;
    
    num_of_elements(v1, ele_number);
    for (unsigned int i=0; i<v1.size(); i++)
        cout<<v1[i]<<" ";
    cout<<"\n"<<v1.capacity();
    
    cout<<"unesite donju i gornju granicu";
    cin>>a>>b;
    interval(v2, a, b);
    for (unsigned int i=0; i<v2.size(); i++)
        cout<<v2[i]<<" ";
    cout<<"\n"<<v2.capacity();
    
    third_vector (v1, v2, v3, ele_number);
    cout<<"\n";
    for (unsigned int i=0; i<v3.size(); i++)
        cout<<v3[i]<<" ";
    cout<<"\n"<<v3.capacity();
    
    return 0;
}

