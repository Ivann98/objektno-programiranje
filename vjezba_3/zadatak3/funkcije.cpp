#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include "header.h"

using namespace std;

void sort_funk(vector<int> &v1)
{
    sort(v1.begin(), v1.end());
}

void insert_funk(vector<int> &v1)
{
    v1.insert(v1.begin(), 0);
}

void sum_funk(vector<int> &v1)
{
    int sum=0;
    for (int i=0; i<v1.size(); i++)
        sum+=v1[i];
    v1.push_back(sum);
}

