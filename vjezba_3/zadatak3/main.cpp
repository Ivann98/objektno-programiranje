#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include "header.h"

int main()
{
    vector<int> v1{ 3, 5, 1, 11, 10, 45, 7 };
    sort_funk(v1);
    insert_funk(v1);
    sum_funk(v1);
    for (unsigned int i=0; i<v1.size(); i++)
        cout<<v1[i]<<" ";
    cout<<"\n"<<v1.capacity();
    return 0;
}
