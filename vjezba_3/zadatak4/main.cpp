#include <iostream>
#include <vector>
#include "header.h"
#include <string>
#include <bits/stdc++.h>

using namespace std;


int main()
{
    vector<int> v{ 11, 4, 7, 18, 34, 1, 6 };
    int x;
    cout<<"Unesite element kojeg zeliti izbaciti iz vektora";
    cin>>x;
    remove_elements(v, x);
    for (int i=0; i<v.size(); i++)
        cout<<v[i]<<" ";
    return 0;
}
