#include <iostream>
#include <vector>
#include <cstring>
#include <bits/stdc++.h>
#include "header.h"

using namespace std;


void sort_and_reverse (string s, vector<string> &v)
{
    reverse(s.begin(), s.end());
    v.push_back(s);
    sort(v.begin(), v.end());
}

