#include <iostream>
#include <cmath>

using namespace std;

class oruzje{
    public:
        int pozicijax, pozicijay;
        int br_metaka_punje; //mzd const moze
        int br_metaka_tren;

        void pucanje (int &br_metaka_tren, int br_metaka_punje);
        int reload (int &br_metaka_tren, int br_metaka_punje);
};


void oruzje::pucanje (int &br_metaka_tren, int br_metaka_punje) 
{
    if (br_metaka_tren!=0)
        br_metaka_tren-1;
    else if (br_metaka_tren==0){
    	cout<<"ostali ste bez metaka, slijedi punjenje\n";
    	br_metaka_tren=oruzje::reload(br_metaka_tren, br_metaka_punje); 
		}
}

int oruzje::reload (int &br_metaka_tren, int br_metaka_punje)
{
    return br_metaka_punje;
}

class meta{
    public:
		int gdx, gdy;
    	int dlx, dly;
    
    	bool pogoden=true;
    	bool promasaj=false;
};

int main()
{
    int i, br=0;
    oruzje pucac;
    meta mete;
    cout<<"Unesite poziciju oruzja, broja metaka u punjenju i trenutni broj metaka:"<<endl;
    cin>>pucac.pozicijax>>pucac.pozicijay;
    cin>>pucac.br_metaka_punje;
    cin>>pucac.br_metaka_tren;
    if (pucac.br_metaka_tren==0)
    	pucac.pucanje(pucac.br_metaka_tren, pucac.br_metaka_punje);
	int n;
    cout<<"Unesite broj meta (n):"<<endl;
    cin>>n;
	for (i=0; i<n; i++)
    {
		cout<<"Unesite granice za "<<i+1<<" metu"<<endl;
        cin>>mete.gdx>>mete.gdy;
        cin>>mete.dlx>>mete.dly;
		
   		pucac.pucanje(pucac.br_metaka_tren, pucac.br_metaka_punje);
    
		if ((mete.gdy==pucac.pozicijay) || (mete.dly==pucac.pozicijay)){
    		cout<<mete.pogoden;
			br++;
		}
		else
    		cout<<mete.promasaj;
	}
    
	cout<<"Broj pogodenih meta "<<br;
	return 0;
}
