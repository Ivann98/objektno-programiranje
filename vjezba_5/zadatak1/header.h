#ifndef HEADER_H
#define HEADER_H
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include "header.h"

using namespace std;


class pozicija{
    public:
        double x, y, z;

        void set (double &x, double &y, double &z);
        void setrand (int a, int b, double &x, double &y, double &z);
        double getvaluex() const { return x; } 
        double getvaluey() const { return y; }
        double getvaluez() const { return z; }
        double udaljenost2D(double x, double y, double x1, double y1);
        double udaljenost3D(double x, double y, double z, double x1, double y1, double z1);
};


/*void pozicija::set (double &x, double &y, double &z);

void pozicija::setrand (int dg, int gg, double &x, double &y, double &z);

double pozicija::udaljenost2D (double x1, double x2, double y1, double y2);

double pozicija::udaljenost3D (double x1, double x2, double y1, double y2, double z1, double z2);
*/

#endif
