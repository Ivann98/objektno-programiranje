#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include "header.h"

using namespace std;

int main()
{
    pozicija var;
    var.x=0;
    var.y=0;
    var.z=0;

    double dvaD, triD, x, y, z;
    int dg, gg;
    
    var.set(var.x, var.y, var.z);
    cout<<"x, y i z su:"<<endl<<var.x<<endl<<var.y<<endl<<var.z<<endl;
    
    cout<<"Unesite gornju i donju granicu: "<<endl;
    cin>>dg>>gg;
    var.setrand(dg, gg, var.x, var.y, var.z);
    cout<<"Random generirani brojojevi za x, y i z su:"<<var.x<<endl<<var.y<<endl<<var.z<<endl;

    x=var.getvaluex();
    cout<<"Vrijednost varijable x: "<<x<<endl;
    
    y=var.getvaluey();
    cout<<"Vrijednost varijable y: "<<y<<endl;
    
    z=var.getvaluez();
    cout<<"Vrijednost varijable z: "<<z<<endl;

    dvaD=var.udaljenost2D(2.0, 5.0, 4.0, 7.0);
    cout<<"Udaljenost 2D tocki je: "<<dvaD<<endl;

    triD=var.udaljenost3D(2.0, 5.0, 4.0, 7.0, 1.0, 6.0);
    cout<<"Udaljenost 3D tocki je: "<<triD<<endl;

}
