#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>
#include<bits/stdc++.h>
#include "header.h"

using namespace std;

class HangmanModel{
public:
    string movie;
    string guessMovie;
    int lives=8;
    bool inGame;

};


class HangmanView{
public:
    char CurrentProgress(HangmanModel &m);
    void UsedLetter(char c, string& str);
    void displyHangman(HangmanModel m);
};

class HangmanController{
public:
    char userEntry();
    int checkLetter(HangmanModel &m, HangmanView v);
    void updateLives(HangmanModel& m, HangmanView v);
    bool checkIfGameIsOver(HangmanModel &m);

};

void HangmanView::displyHangman(HangmanModel m)
{
    if (m.lives<=7)
        cout<<"     |   "<<endl;
    if (m.lives<=6)
        cout<<"     O   "<<endl;
    if (m.lives<=5)
        cout<<"    /";
    if (m.lives<=4)
        cout<<"|";
    if (m.lives<=3)
        cout<<"\\"<<endl;
    if (m.lives<=2)
        cout<<"     |"<<endl;
    if (m.lives<=1)
        cout<<"    / ";
    if (m.lives<=0)
        cout<<"\\" <<endl;
}

char HangmanView::CurrentProgress(HangmanModel &m)
{
    cout<<"Pogodena rijec sada izgleda: "<<m.guessMovie<<endl;
}


void HangmanView::UsedLetter(char c, string &str)
{
    str+=c;
    cout<<"Promasena slova do sad su: "<<str<<endl;
}


void HangmanController::updateLives(HangmanModel &m, HangmanView v)
{
    m.lives=m.lives-1;
    cout<<m.lives<<endl;
    v.displyHangman(m);
}


bool HangmanController::checkIfGameIsOver(HangmanModel &m)
{
    if (m.lives==0){
        cout<<"Izgubili ste sve zivote, igra je gotova!"<<endl;
        return 0;
    }
    int i=m.movie.compare(m.guessMovie);
    if (i==0){
        cout<<"Pobjeda! Ime filma pogodeno!"<<endl;
        return 0;
    }
}

char HangmanController::userEntry()
{
    char letter;
    cout<<"\nUnesite (malo) slovo za provjeru"<<endl;
    cin>>letter;
    return letter;
}


int HangmanController::checkLetter (HangmanModel &m, HangmanView v)
{
    string str;
    char c;
    for (int i=0; m.movie[i]!='\0'; i++)
    {
       if (isspace(m.movie[i]))
            m.guessMovie+=" ";
        else
            m.guessMovie+="_";

    }
    cout<<m.guessMovie<<endl;
    int i=0;
    while (m.lives!=0){
        bool flag=false;
        char c=userEntry();
        for (int j=0; j<m.movie.length(); j++){
            if (c==m.movie[j]){

                m.guessMovie[j]=c;
                flag=true;
                }
        }
        if (flag){
            cout<<"Slovo je pogodeno, broj zivota ostao isti!"<<endl;
            cout<<m.lives<<endl;
            v.CurrentProgress(m);
        }
        else{
            cout<<"\nSlovo nije pogodeno, ostalo vam je jos zivota"<<endl;
            //m.guessMovie.insert(i, 1, c);
            HangmanController::updateLives(m, v);
            v.UsedLetter(c, str);

        }
        if (HangmanController::checkIfGameIsOver(m)==0)
            return 1;
    }
    return 0;
}
