#ifndef HEADER_H 
#define HEADER_H
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>
#include<bits/stdc++.h>
#include "header.h"

using namespace std;

class HangmanModel{
public:
    string movie;
    string guessMovie;
    int lives=8;
    bool inGame;

};


class HangmanView{
public:
    char CurrentProgress(HangmanModel &m);
    void UsedLetter(char c, string& str);
    void displyHangman(HangmanModel m);
};

class HangmanController{
public:
    char userEntry();
    int checkLetter(HangmanModel &m, HangmanView v);
    void updateLives(HangmanModel& m, HangmanView v);
    bool checkIfGameIsOver(HangmanModel &m);

};

void HangmanView::displyHangman(HangmanModel m);

char HangmanView::CurrentProgress(HangmanModel &m);

void HangmanView::UsedLetter(char c, string &str);

void HangmanController::updateLives(HangmanModel &m, HangmanView v);

bool HangmanController::checkIfGameIsOver(HangmanModel &m);

char HangmanController::userEntry();

int HangmanController::checkLetter (HangmanModel &m, HangmanView v);




#endif
