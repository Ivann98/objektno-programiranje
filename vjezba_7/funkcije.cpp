#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>
#include<bits/stdc++.h>
#include "header.h"

using namespace std;


struct Point{
public:
    double x, x1;
    double y, y1;
public:
    Point(double x, double y){
        this->x=x;
        this->y=y;
    }
};


class Board{
public:
    int r;
    int c;
    char ch;
    char **matrix;
    void display(Point p, Point p1);
    void draw_char(Point p, char ch);
    void draw_up_line(Point p, char ch);
    void draw_line(Point p, Point p1, char ch);
    Board(){
        r=0;
        c=0;
        ch=' ';
        matrix=0;
    }
    Board(int r, int c, char ch){
        this->r=r;
        this->c=c;
        this->ch=ch;
        int i, j;
        matrix=new char*[r];
        for (int z=0; z<r; z++)
            matrix[z]=new char[c];
        for (i=0; i<r; i++){
            for (j=0; j<c; j++){
                if (i==0 || i==r-1 || j==0 || j==c-1){
                    matrix[i][j]=ch;
                }
                else{
                    matrix[i][j]=' ';
                }
            }
        }
    }

    Board(const Board &ref){
        r=ref.r;
        c=ref.c;
        ch=ref.ch;
        matrix=new char*[r];
        for (int z=0; z<r; z++)
            matrix[z]=new char[c];
    }


    /*~Board(){
        for (int z=0; z<r; z++)
            delete[] matrix[z];
        delete [] matrix;
    }*/

};

void Board::draw_line(Point p, Point p1, char ch){
    int n=int(p.x);
    int m=int(p.y);
    int n1=int(p.x1);
    int m1=int(p.y1);
    for (int i=0; i<r; i++){
        for (int j=0; j<c; j++){
            if (n==i && j==m){
                matrix[i][j]='x';
            if (n>n1 && m1>m){
                n--;
                m++;
                matrix[n][m]='x';
            }
            else if (n>n1 && m1<m){
                n--;
                m--;
                matrix[n][m]='x';
            }
            else if (n<n1 && m1>m){
                n--;
                m++;
                matrix[n][m]='x';
            }
            else if (n<n1 && m1<m){
                n++;
                m--;
                matrix[n][m]='x';
            }
            else if (n==n1){
                if (m>m1){
                    m--;
                    matrix[n][m]='x';
                }
                else{
                    m++;
                    matrix[n][m]='x';
                }
            }
            else if (m==m1){
                if (n>n1){
                    n--;
                    matrix[n][m]=='x';
                }
                else{
                    n++;
                    matrix[n][m]=='x';
                }
            }
        }
    }
}
}

void Board::draw_up_line(Point p, char ch){
    int n=int(p.x);
    int m=int(p.y);
    for (int i=0; i<=c; i++){
        if (i==m){
            for (int j=1; j<=n; j++){
                matrix[j][i]='x';
            }
        }
    }
}

void Board::draw_char(Point p, char ch){
    //cout<<"Unesite tocku x: "<<endl;
    //cin>>p.x;
    p.x=round(p.x);
    //cout<<"Unesite tocku y: "<<endl;
    //cin>>p.y;
    p.y=round(p.y);
    int n=int(p.x);
    int m=int(p.y);
    for (int i=0; i<r; i++){
        for (int j=0; j<c; j++){
            if (i==n && j==m)
                matrix[i][j]='x';
        }
    }
}


void Board::display(Point p, Point p1){

    //draw_char(p, ch);
    //draw_up_line(p, ch);
    draw_line(p, p1, ch);
    for (int i=0; i<=r; i++){
        for (int j=0; j<=c; j++){
            if (i==0 || i==r-1 || j==0 || j==c-1){
                cout<<matrix[i][j];
                //draw_char(p, ch);
            }
            else{
                cout<<matrix[i][j];
                //draw_char(p, ch);
            }
        }
        cout<<endl;
    }
}

