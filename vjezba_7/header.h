#ifndef HEADER_H
#define HEADER_H
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>
#include<bits/stdc++.h>
#include "header.h"


using namespace std;


struct Point{
public:
    double x, x1;
    double y, y1;
public:
    Point(double x, double y){
        this->x=x;
        this->y=y;
    }
};


class Board{
public:
    int r;
    int c;
    char ch;
    char **matrix;
    void display(Point p, Point p1);
    void draw_char(Point p, char ch);
    void draw_up_line(Point p, char ch);
    void draw_line(Point p, Point p1, char ch);
    Board(){
        r=0;
        c=0;
        ch=' ';
        matrix=0;
    }
    Board(int r, int c, char ch){
        this->r=r;
        this->c=c;
        this->ch=ch;
        int i, j;
        matrix=new char*[r];
        for (int z=0; z<r; z++)
            matrix[z]=new char[c];
        for (i=0; i<r; i++){
            for (j=0; j<c; j++){
                if (i==0 || i==r-1 || j==0 || j==c-1){
                    matrix[i][j]=ch;
                }
                else{
                    matrix[i][j]=' ';
                }
            }
        }
    }

    Board(const Board &ref){
        r=ref.r;
        c=ref.c;
        ch=ref.ch;
        matrix=new char*[r];
        for (int z=0; z<r; z++)
            matrix[z]=new char[c];
    }


    /*~Board(){
        for (int z=0; z<r; z++)
            delete[] matrix[z];
        delete [] matrix;
    }*/

};

void Board::draw_line(Point p, Point p1, char ch);

void Board::draw_up_line(Point p, char ch);

void Board::draw_char(Point p, char ch);

void Board::display(Point p, Point p1);

#endif
